package basicWebDriverPrograms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;



public class VerifyUrl {
	
	WebDriver driver;
	@Test
	public void verify() {
		String expectedUrl="amazon";
		System.setProperty(("webdriver.chrome.driver"), "F:\\chromedriver_win32\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://www.amazon.in/");
		driver.manage().window().maximize();
		String actual=driver.getCurrentUrl();
		
		if(actual.equalsIgnoreCase(expectedUrl)) {
			System.out.println("test case passed");
		}
		else
			System.out.println("failed");
		
		driver.quit();
		
		
		
		
		
	}
	


}
